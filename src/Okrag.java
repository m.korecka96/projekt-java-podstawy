public class Okrag {

    Double promien;

    Okrag(Double promienOkregu){
        this.promien=promienOkregu;
    }

    public Double obwod(){
        return 2*Math.PI*promien;
    }
    public Double pole(){
        return Math.PI*(Math.pow(promien, 2));
    }
}
