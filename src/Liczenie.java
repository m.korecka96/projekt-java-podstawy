public class Liczenie {


    public static int dodawanie(int[] tablica) {
        int suma=0;

        for(int i=0; i<tablica.length;i++){

            suma = suma + tablica[i];
        }
        return suma;
    }
    public static int odejmowanie(int[] tablica) {
        int roznica=0;

        for(int i=0; i<tablica.length;i++){

            roznica = roznica - tablica[i];
        }
        return roznica;
    }
    public static int mnozenie(int[] tablica) {
        int iloczyn=1;

        for(int i=0; i<tablica.length;i++){

            iloczyn = iloczyn * tablica[i];
        }
        return iloczyn;
    }
    public static int dzielenie(int[] tablica) {
        int iloraz=tablica[0];

        for(int i=1; i<tablica.length;i++){

            iloraz = iloraz / tablica[i];
        }
        return iloraz;
    }
    public static int modulo(int[] tablica) {
        int reszta=tablica[0];

        for(int i=1; i<tablica.length;i++){

            reszta %= tablica[i];
        }
        return reszta;
    }
}
