public class PracownikMain {

    public static int srednieZarobki(String[][] tablica, String numerDzialu, String plec) {
        int suma = 0;
        int liczba=0;

        for(int i = 0; i < tablica.length; i++){

            if(tablica[i][3].equalsIgnoreCase(plec) && tablica[i][4].equalsIgnoreCase(numerDzialu)) {

                suma = suma + Integer.parseInt(tablica[i][2]);
                liczba = liczba + 1;

            }
        }

        int srednia = suma/liczba;

        return srednia;
    }


    public static void main(String[] args) {
        String[][] tablica = new String[5][];
        tablica[0] = new String[]{"Jan", "Kowalski", "2010", "M" ,"3"};
        tablica[1] = new String[]{"Agnieszka", "Cuber", "2900", "K", "3"};
        tablica[2] = new String[]{"Adam",  "Nowak",  "1100",  "M", "4"};
        tablica[3] = new String[]{"Sylwia", "Zych", "2100", "K", "3"};
        tablica[4] = new String[]{"Beata", "Dudek", "1900", "K", "4"};

        System.out.println(srednieZarobki(tablica, "3", "K"));
    }
}
