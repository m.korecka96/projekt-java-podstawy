public class Samochod {

    private String marka;
    private String rodzaj;
    private int rokProdukcji;

//    // konstruktor argumentowy
//    public Samochod(String marka, String rodzaj, int rokProdukcji) {
//        this.marka = marka;
//        this.rodzaj = rodzaj;
//        this.rokProdukcji = rokProdukcji;
//    }

    // gettery
    public String getMarka() {

        return marka;
    }
    public String getRodzaj(){
        return rodzaj;
    }
    public int getRokProdukcji(){
        return rokProdukcji;
    }
    // settery
    public void setMarka(final String marka){
        this.marka=marka;
    }

    public void setRodzaj(final String rodzaj){
        this.rodzaj=rodzaj;
    }
    public void setRokProdukcji(final int rokProdukcji){
        this.rokProdukcji= rokProdukcji;
    }

    // zwrot stringów
    public String toString(){
        return  "Marka to " + marka + ", "+
                "Rodzaj to " + rodzaj +
                " ,wyprodukowano w " + rokProdukcji;
    }

}

//Samochod Toyota = new Samochod('Toyota','osobowy',2011);
//
//Toyota.getRodzaj();
