public class Pracownik {
    private String imie;
    private String nazwisko;
    private int placa;
    private char plec;
    private int dzial;

    public Pracownik(String imie, String nazwisko, int placa, char plec, int dzial){
        this.imie=imie;
        this.nazwisko=nazwisko;
        this.placa=placa;
        this.plec=plec;
        this.dzial=dzial;
    }
}
