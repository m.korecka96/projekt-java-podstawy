import java.util.Scanner;

// zadanie 2
public class BMI {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Podaj swoja wage: ");
        float waga = scan.nextFloat();

        System.out.println("Podaj swoj wzrost: ");
        int wzrost = scan.nextInt();

        double wzrostWMetrach = wzrost / 100.0;
        double bmi = (waga / Math.pow(wzrostWMetrach, 2));

        if (bmi < 18.5) {
            System.out.println("BMI nieoptymalne, za niskie " + bmi);
        } else if (bmi > 24.9) {
            System.out.println("BMI nieoptymalne, za wysokie " + bmi);
        } else {
            System.out.println("BMI optymalne " + bmi);
        }
    }

}


