import java.util.Scanner;
// zadanie 4
public class PifPaf {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbę: ");
        int liczba = scan.nextInt();


        for (int i = 1; i <= liczba; i++) {

            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("Pif Paf");
            } else if (i % 3 == 0) {
                System.out.println("Pif");
            } else if (i % 7 == 0) {
                System.out.println("Paf");
            } else {
                System.out.println(i);
            }

        }

    }
}
