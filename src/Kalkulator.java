import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        // Podaj ile liczb chcesz wprowadzić (min. 2 liczby)
        // Wprowadź liczby
        // Jakie działanie chcesz wykonać
        // Liczenie w odzielnej klasie i to obiekt tej klasy zwraca rezultat.

        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz wprowadzić: ");
        int ile = scan.nextInt();

        int[] liczby = new int[ile];

        for (int i=0; i<ile; i++){
            System.out.printf("Podaj %s liczbę: \n",i+1);
            liczby[i]=scan.nextInt();
        }

        scan.nextLine();


        System.out.println("Jakie działanie chcesz wykonać? ");
        String operacja = scan.nextLine();

        if (operacja.equalsIgnoreCase("dodawanie")) {
            System.out.printf("Wynik to: %s", Liczenie.dodawanie(liczby));
        }
        else if (operacja.equalsIgnoreCase("odejmowanie")) {
            System.out.printf("Wynik to: %s", Liczenie.odejmowanie(liczby));
        }
        else if (operacja.equalsIgnoreCase("mnozenie")) {
            System.out.printf("Wynik to: %s", Liczenie.mnozenie(liczby));
        }
        else if (operacja.equalsIgnoreCase("dzielenie")) {
            System.out.printf("Wynik to: %s", Liczenie.dzielenie(liczby));
        }
        else if (operacja.equalsIgnoreCase("modulo")) {
            System.out.printf("Wynik to: %s", Liczenie.modulo(liczby));
        }
        else
            System.out.println("Bledna metoda!");
    }
}
